name := "image-render-service"

version := "0.1"

scalaVersion := "2.12.4"

scalacOptions += "-Ypartial-unification"

libraryDependencies ++= Seq(
  "org.typelevel"     %% "cats-core"   % "1.1.0"
)