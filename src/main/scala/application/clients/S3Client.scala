package application.clients

import java.io.File

import scala.concurrent.{ExecutionContext, Future}
import scala.util.control.NonFatal

import application.models.{GeneratedImage, ImageProcessingResult, S3UploadError}

class S3Client(config: Seq[String])(implicit ec: ExecutionContext) {
  def uploadToS3(from: File, to: String, data: GeneratedImage): Future[ImageProcessingResult] = Future {
    ImageProcessingResult(fromFile = from.getAbsolutePath, toS3 = Some(s"/s3/bucket/${data.name}"))
  }.recover {
    case NonFatal(_) => ImageProcessingResult(from.getName, error = Some(S3UploadError(from.getName)))
  }
}
object S3Client {
  def apply()(implicit ec: ExecutionContext): S3Client = new S3Client(Nil)
}
