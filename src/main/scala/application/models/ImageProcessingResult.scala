package application.models

case class ImageProcessingResult(
  fromFile: String,
  toS3: Option[String] = None,
  error: Option[ImageProcessingError] = None
)
