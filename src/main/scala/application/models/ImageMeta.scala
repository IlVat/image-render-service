package application.models

import java.nio.file.Path

case class Image(path: Path, meta: ImageMeta)

case class ImageMeta(
  designerId: Int,
  designId: Int,
  productType: ProductType,
  orientation: Orientation,
  size: Size
)

case class ImageScaleInfo(
  image: Image,
  scaleToSize: Size
)

case class GeneratedImage(data: String, name: String)

// ProductType could be ADT or enumeration,
// but the list of the data could be long, so it is better to have a case class
case class ProductType(code: String)

sealed abstract class Orientation(val code: String)

object Orientation {
  case object Portrait extends Orientation("P")
  case object Landscape extends Orientation("L")
  case object Square extends Orientation("X")
  case object Unknown extends Orientation("U")
  
  def codes = List(Portrait, Landscape, Square)
  def decode(code: String): Option[Orientation] = codes.find(_.code == code)
}

case class Size(width: Int, height: Int)
