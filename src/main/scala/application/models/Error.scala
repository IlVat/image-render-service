package application.models

sealed abstract class ImageProcessingError(val value: String, val fileName: String)

case class ImageParseError(override val fileName: String)
  extends ImageProcessingError(value = "image.parse.error", fileName = fileName)

case class ImageDatabaseError(override val fileName: String)
  extends ImageProcessingError(value = "image.database.error", fileName = fileName)

case class ImageDeleteFileError(override val fileName: String)
  extends ImageProcessingError(value = "image.create.file.error", fileName = fileName)

case class ImageCreateFileError(override val fileName: String)
  extends ImageProcessingError(value = "image.delete.file.error", fileName = fileName)

case class ImageReadFileError(override val fileName: String)
  extends ImageProcessingError(value = "image.read.file.error", fileName = fileName)

case class ImageScalingError(override val fileName: String)
  extends ImageProcessingError(value = "image.scaling.file.error", fileName = fileName)

case class S3UploadError(override val fileName: String)
  extends ImageProcessingError(value = "image.s3.upload.error", fileName = fileName)
