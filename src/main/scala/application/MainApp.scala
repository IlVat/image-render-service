package application

import java.io.File
import java.util.concurrent.Executors

import scala.concurrent.ExecutionContext
import scala.util.Success

import application.clients.S3Client
import application.db.ImageDao
import application.services.{FileService, ImageService}

object MainApp extends App {
  
  val imageDao = {
    implicit val ec = ExecutionContext.fromExecutor(Executors.newFixedThreadPool(10))
    new ImageDao()
  }
  
  val s3Client = {
    implicit val ec = ExecutionContext.fromExecutor(Executors.newFixedThreadPool(10))
    S3Client()
  }
  
  val fileService = {
    implicit val ec = ExecutionContext.fromExecutor(Executors.newFixedThreadPool(10))
    new FileService()
  }
  
  val imageService = {
    implicit val ec = ExecutionContext.fromExecutor(Executors.newFixedThreadPool(10))
    new ImageService(imageDao, fileService, s3Client)
  }
  
  val seq = List(
    "C:\\Projects\\image-render-service\\tmp_images\\0-3-100L-60x90.jpg",
    "C:\\Projects\\image-render-service\\tmp_images\\0-3-100L-WRONGx90.jpg",
  )
  
  import scala.concurrent.ExecutionContext.Implicits.global
  
  imageService
    .run(seq.map(x => new File(x)))
    .onComplete {
      case Success(r) =>
        r.foreach(println)
        System.exit(1) //just for test
      case _ =>
        System.exit(1)
    }
}
