package application.db

import scala.concurrent.{ExecutionContext, Future}

object Database {
  //just mock
  //Portrait/landscape sizes are: 20x30, 40x60, 60x90, 80x120, 100x150
  //Square sized are: 20x20, 30x30, 50x50, 70x70, 100x100
  
  val sizes = Map(
    "L" -> List((20, 30), (40, 60), (60, 90), (80, 120), (100, 150)),
    "P" -> List((20, 30), (40, 60), (60, 90), (80, 120), (100, 150)),
    "X" -> List((20, 20), (30, 30), (50, 50), (70, 70), (100, 100))
  )
}

//just simple implementation
class ImageDao()(implicit ec: ExecutionContext) {
  //Emulating async query to DB
  def getSizesBy(o: String)(p: (Int, Int) => Boolean): Future[List[(Int, Int)]] = {
    Future { Database.sizes.getOrElse(o, List.empty).filter{case (x,y) => p(x, y)} }
  }
}
