package application.utils

import application.models.Orientation.Unknown
import application.models._

object ImageParser {
  val imagePattern = """(\d{1,10})-(\d{1,10})-(\d{3,4})([PLX])-(\d{2,3})x(\d{2,3})\.jpg""".r
  
  def parse(fileName: String): Either[ImageProcessingError, ImageMeta] = fileName match {
    case imagePattern(designerId, designId, pType, orientation, width, height) =>
      Right(ImageMeta(
        designerId.toInt,
        designId.toInt,
        ProductType(pType),
        Orientation.decode(orientation).getOrElse(Unknown),
        Size(width.toInt, height.toInt)))
    case _ => Left(ImageParseError(fileName))
  }
  
}
