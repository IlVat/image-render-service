package application.services

import java.io.File
import java.nio.file.{Files, Path}

import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success, Try}
import cats.syntax.either._
import application.models.{ImageCreateFileError, ImageDeleteFileError, ImageProcessingError, ImageReadFileError}

class FileService()(implicit ec: ExecutionContext) {
  def createTemporaryFile(file: File): Future[Either[ImageProcessingError, Path]] = Future {
    Try(Files.createTempFile(file.toPath.getParent, "tmp-", ".jpg")) match {
      case Success(r) => r.asRight
      case Failure(_) => ImageCreateFileError(file.getName).asLeft
    }
  }
  
  def readFile(file: File): Future[Either[ImageReadFileError, String]] = Future {
    Try("faking read from file") match {
      case Success(r) => r.asRight
      case Failure(_) => ImageReadFileError(file.getName).asLeft
    }
  }
  
  def deleteTemporaryFile(file: File): Future[Either[ImageProcessingError, Boolean]] = Future {
    Try(Files.deleteIfExists(file.toPath)) match {
      case Success(r) => r.asRight
      case Failure(_) => ImageDeleteFileError(file.getName).asLeft
    }
  }
}
