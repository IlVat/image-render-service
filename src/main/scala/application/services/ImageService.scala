package application.services

import java.io.File
import java.nio.file.Path
import java.time.Instant

import scala.concurrent.{ExecutionContext, Future}
import scala.util.control.NonFatal

import application.clients.S3Client
import application.db.ImageDao
import application.models._
import application.services.ImageService.ResultOrError
import application.utils.ImageParser
import cats.Traverse
import cats.data.EitherT
import cats.syntax.either._
import cats.syntax.traverse._
import cats.instances.list._
import cats.instances.either._
import cats.instances.future._

object ImageService {
  type ResultOrError[T] = EitherT[Future, ImageProcessingError, T]
  
  def apply(imageDao: ImageDao,
            fileService: FileService,
            s3Client: S3Client)
           (implicit ec: ExecutionContext): ImageService = new ImageService(imageDao, fileService, s3Client)
}

class ImageService(imageDao: ImageDao, fileService: FileService, s3Client:S3Client)(implicit ec: ExecutionContext) {
  
  def run(files: List[File]): Future[List[ImageProcessingResult]] = {
    files.flatTraverse[Future, ImageProcessingResult](processOne)
  }
 
  def scale(input: File, size: Size): Future[Either[ImageScalingError, GeneratedImage]] = {
    //faking behaviour
    fileService.readFile(input)
      .map(_ => GeneratedImage("generated", s"image-${Instant.now}-${size.width}x${size.height}").asRight)
      .recover({ case NonFatal(_) => ImageScalingError(input.getName).asLeft })
  }
  
  private def processOne(file: File): Future[List[ImageProcessingResult]] = {
    val chain = for {
      im      <- EitherT.fromEither[Future](createImage(file))
      sizes   <- getSizes(im)
      tmpPath <- createTemporaryFile(file)
      data    <- scaleImage(sizes, tmpPath)
      result  <- uploadImagesToS3(file, "/s3/bucket/", data)
      _       <- deleteTemporaryFile(tmpPath.toFile)
    } yield {
      result
    }
    
    chain.value.map {
      case Left(err) => List(ImageProcessingResult(file.getName, error = Some(err)))
      case Right(r) => r
    }
  }
  
  private def createImage(file: File): Either[ImageProcessingError, Image] = {
    val meta = ImageParser.parse(file.getName)
    meta.map(m => Image(new File(file.getAbsolutePath).toPath, m))
  }
  
  private def getSizes(image: Image): ResultOrError[List[ImageScaleInfo]] = {
    val width = image.meta.size.width
    val height = image.meta.size.height
    val orientation = image.meta.orientation.code
    val sizesF = imageDao.getSizesBy(orientation)((w, h) => w < width && h < height)
    
    val result = sizesF.map { s =>
      s.map { case (w, h) => ImageScaleInfo(image, Size(w, h)) }.asRight
    }.recover {
      case NonFatal(_) => ImageDatabaseError(image.path.toFile.getName).asLeft
    }
  
    EitherT[Future, ImageProcessingError, List[ImageScaleInfo]](result)
  }
  
  private def scaleImage(sizes: List[ImageScaleInfo],
                         tmpPath: Path): ResultOrError[List[GeneratedImage]] =
  {
    val scaleF = sizes.traverse[Future, Either[ImageProcessingError, GeneratedImage]] { i =>
      scale(tmpPath.toFile, i.scaleToSize)
    }
    EitherT[Future, ImageProcessingError, List[GeneratedImage]](scaleF.map(Traverse[List].sequence(_)))
  }
  
  private def uploadImagesToS3(file: File,
                               s3Path:String,
                               data: List[GeneratedImage]): ResultOrError[List[ImageProcessingResult]] =
  {
    EitherT.liftF[Future, ImageProcessingError,List[ImageProcessingResult]]{
      data.traverse[Future,ImageProcessingResult](d => s3Client.uploadToS3(file,"/s3/bucket/", d))
    }
  }
  
  private def createTemporaryFile(file: File): ResultOrError[Path] = {
    EitherT[Future, ImageProcessingError, Path](fileService.createTemporaryFile(file))
  }
  
  private def deleteTemporaryFile(file: File): ResultOrError[Boolean] = {
    EitherT[Future, ImageProcessingError, Boolean](fileService.deleteTemporaryFile(file))
  }
}
